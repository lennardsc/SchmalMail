import tkinter as tk
from tkinter import messagebox, Listbox, Scrollbar, Text, END, OptionMenu
import imaplib
import email.parser
import keyring
import html2text

class EmailClientApp:
    def __init__(self, master):
        self.master = master
        master.title("Email Client App")

        # Check if credentials are stored
        self.providers = self.get_stored_providers()

        # If providers are found, automatically log in and show provider dropdown
        if self.providers:
            self.provider_label = tk.Label(master, text="Select Email Provider:")
            self.provider_label.pack()

            self.provider_var = tk.StringVar(master)
            self.provider_var.set(self.providers[0])  # Set default value
            self.provider_dropdown = OptionMenu(master, self.provider_var, *self.providers)
            self.provider_dropdown.pack()

            self.login()
        else:
            self.show_login_screen()

    def get_stored_providers(self):
    # Retrieve stored providers from keyring
        providers = keyring.get_password("email_providers", "providers")
        return providers.split(",") if providers else []

    def show_login_screen(self):
        # Left Frame for Email List
        self.left_frame = tk.Frame(self.master)
        self.left_frame.pack(side=tk.LEFT, padx=10, pady=10)

        self.label = tk.Label(self.left_frame, text="Email List:")
        self.label.pack()

        self.email_listbox = Listbox(self.left_frame, width=60, height=20)
        self.email_listbox.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

        self.scrollbar = Scrollbar(self.left_frame, orient=tk.VERTICAL)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

        self.email_listbox.config(yscrollcommand=self.scrollbar.set)
        self.scrollbar.config(command=self.email_listbox.yview)

        # Right Frame for Email Body
        self.right_frame = tk.Frame(self.master)
        self.right_frame.pack(side=tk.RIGHT, padx=10, pady=10)

        self.email_body_text = Text(self.right_frame, width=60, height=20)
        self.email_body_text.pack(fill=tk.BOTH, expand=True)

        # Login Inputs
        self.provider_label = tk.Label(self.master, text="Enter Email Provider:")
        self.provider_label.pack()

        self.provider_entry = tk.Entry(self.master)
        self.provider_entry.pack()

        self.username_label = tk.Label(self.master, text="Email Address:")
        self.username_label.pack()

        self.username_entry = tk.Entry(self.master)
        self.username_entry.pack()

        self.password_label = tk.Label(self.master, text="Password:")
        self.password_label.pack()

        self.password_entry = tk.Entry(self.master, show="*")
        self.password_entry.pack()

        self.login_button = tk.Button(self.master, text="Login", command=self.login)
        self.login_button.pack()

    def login(self):
        provider = self.provider_var.get() if self.providers else self.provider_entry.get().strip()
        username = self.username_entry.get().strip()
        password = self.password_entry.get().strip()

        if not provider or not username or not password:
            messagebox.showerror("Error", "Please fill in all fields")
            return

        try:
            # Set IMAP and SMTP server based on the provider
            if "gmail" in provider.lower():
                imap_server = "imap.gmail.com"
                smtp_server = "smtp.gmail.com"
            elif "yahoo" in provider.lower():
                imap_server = "imap.mail.yahoo.com"
                smtp_server = "smtp.mail.yahoo.com"
            elif "outlook" in provider.lower():
                imap_server = "outlook.office365.com"
                smtp_server = "smtp-mail.outlook.com"
            elif "mailbox.org" in provider.lower():
                imap_server = "imap.mailbox.org"
                smtp_server = "smtp.mailbox.org"

            # Store credentials securely using keyring
            keyring.set_password(provider, username, password)

            # Example: IMAP connection to check for new emails
            mail = imaplib.IMAP4_SSL(imap_server)
            mail.login(username, password)
            mail.select('inbox')
            typ, data = mail.search(None, 'ALL')

            for num in data[0].split():
                typ, msg_data = mail.fetch(num, '(RFC822)')
                raw_email = msg_data[0][1]
                parser = email.parser.BytesParser()
                msg = parser.parsebytes(raw_email)
                subject = msg.get('Subject', 'No Subject')
                sender = msg.get('From', 'Unknown Sender')
                self.email_listbox.insert(tk.END, f"{sender}: {subject}")

            mail.close()
            mail.logout()

            messagebox.showinfo("Success", "Login successful!")

        except Exception as e:
            messagebox.showerror("Error", f"Login failed: {str(e)}")

    def show_email_content(self, event):
        selected_index = self.email_listbox.curselection()
        if selected_index:
            selected_index = int(selected_index[0])
            email_info = self.email_listbox.get(selected_index)
            # Clear previous content
            self.email_body_text.delete(1.0, tk.END)

            try:
                # Fetch email content
                mail = imaplib.IMAP4_SSL(self.imap_server)
                mail.login(self.username, self.password)
                mail.select('inbox')
                typ, data = mail.search(None, 'ALL')
                num = data[0].split()[selected_index]
                typ, msg_data = mail.fetch(num, '(RFC822)')
                raw_email = msg_data[0][1]
                parser = email.parser.BytesParser()
                msg = parser.parsebytes(raw_email)
                email_body = msg.get_payload()

                # Convert HTML content to plain text
                if msg.is_multipart():
                    for part in msg.walk():
                        content_type = part.get_content_type()
                        if content_type == "text/plain":
                            email_body = part.get_payload(decode=True).decode()
                        elif content_type == "text/html":
                            email_body = part.get_payload(decode=True).decode()
                            email_body = html2text.html2text(email_body)
                            break

                # Display email content
                self.email_body_text.insert(tk.END, email_body)
                mail.close()
                mail.logout()
            except Exception as e:
                self.email_body_text.insert(tk.END, f"Error fetching email content: {str(e)}")

root = tk.Tk()
app = EmailClientApp(root)
root.mainloop()
